/**
 * Function to see if your choice is the right answer 
 *(to determine if you get a point or not in the end)
 *@ubaid Ur Rehman
 *Student#991395773
 *Date:2020/06/08
 */

function Question(text, choices, answer) {
    this.text = text;
    this.choices = choices;
    this.answer = answer;
}

Question.prototype.isCorrectAnswer = function(choice) {
    return this.answer === choice;
};