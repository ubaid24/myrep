/** 
 * Contains functions for the Trivia, progress, score, questions ect..
 * 
 *@ubaid Ur Rehman
 *Student#991395773
 *Date:2020/06/08
 */


//will show score after Trivia ends
function QuizMaker () {
    if(quiz.isEnded()) {
        showScores();
    }
    //Go to next question until Trivia finished
    else {
       
        var element = document.getElementById("question");
        element.innerHTML = quiz.getQuestionIndex().text;

      //shows options for question
        var choices = quiz.getQuestionIndex().choices;
        for(var i = 0; i < choices.length; i++) {
            var element = document.getElementById("choice" + i);
            element.innerHTML = choices[i];
            guess("btn" + i, choices[i]);
        }

        showProgress();
    }
};
//updates score in background
function guess(id, guess) {
    var button = document.getElementById(id);
    button.onclick = function() {
        quiz.guess(guess);
        QuizMaker();
    };
}
//Show page progress
function showProgress() {
    var currentQuestionNumber = quiz.questionIndex + 1;
    var element = document.getElementById("progress");
    element.innerHTML = "Question " + currentQuestionNumber + " of " + quiz.questions.length;
};
//Show score at the end
function showScores() {
    var gameOverHTML = "<h1>Result</h1>";
    gameOverHTML += "<h2 id='score'> Your scores: " + quiz.score + "</h2>";
    var element = document.getElementById("quiz");
    element.innerHTML = gameOverHTML;
};

//Creates questions
var questions = [
    new Question("What is the capital of Canada?",
    ["Toronto", "Ottawa","Halifax", "Montreal"], "Ottawa"),
    
    new Question("O'Canada! our home and native land!(finish the next line)",
    ["Let morning shine on the silver and gold of this land",
     "True patriot love in all of us command",
     "O'er the land of the free and the home of the brave?",
     "Shine in glory everlasting"],
     "True patriot love in all of us command"),
     
     
    new Question("what year was Canada founded?",
    ["1867", "1945","1815", "1783"], "1867"),
    
    new Question("What is the symbol for the Canadian coat of arms?",
    ["Bald eagle", "Shield and crown",
     "Cresent and star",
     "Red star and Hydroelectric plant"], "Shield and crown"),
 
    new Question("Who was Canada's first prime minister?",
    ["Justin Trudeau", "John A. Macdonald", "Alexander Mackenzie",
     "Wilfrid Laurier"], "John A. Macdonald")
];

// Creates Trivia
var quiz = new Quiz(questions);

QuizMaker();