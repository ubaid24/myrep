/**
 * 
 * 
 *@ubaid Ur Rehman
 *Student#991395773
 *Date:2020/06/08
 */
//start score count at 0 and display question index
function Quiz(questions) {
    this.score = 0;
    this.questions = questions;
    this.questionIndex = 0;
}
Quiz.prototype.getQuestionIndex = function() {
    return this.questions[this.questionIndex];
};
//If you get the right answer, your score will increment by 1
Quiz.prototype.guess = function(answer) {
    if(this.getQuestionIndex().isCorrectAnswer(answer)) {
        this.score++;
    }

    this.questionIndex++;
};

Quiz.prototype.isEnded = function() {
    return this.questionIndex === this.questions.length;
};